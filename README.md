# sqlalchemy_example
## Getting started
All dependencies installation: 

`pip install -r requirements.txt`


Execute most scripts as:

`python some_script.py --host db-host --port db-port --username user --password password --database database_name`

Script _show_items_in_shop.py_ can be called:

`python ./show_items_in_shop.py --host db-host --port db-port  --username user --password password --database db_name --shop_name "Shop name" --shop_address "Shop Address"`

Script _update_item_by_id.py_ can be executed as:

`python ./update_item_by_id.py --host db-host --port db-port --username username --password password --database db_name --id itemId --field_name ITEM_NAME|PRICE --value value`
