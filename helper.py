import pandas


def read_csv(path_to_csv):
    return tuple(dict(item[1]) for item in tuple(pandas.read_csv(path_to_csv, sep=';', header=0, index_col = None).iterrows()))