import allure
import pandas
from sqlalchemy import create_engine, MetaData, Table
from sqlalchemy.orm import sessionmaker
from sqlalchemy_utils import drop_database


class MySQLAdapter:
    def __init__(self, host, port, user, password, database):
        self.__engine = create_engine(f'mysql+pymysql://{user}:{password}@{host}:{str(port)}/{database}')
        Session = sessionmaker(bind=self.__engine)
        self.__session = Session()
        self.__metadata = MetaData(bind=self.__engine)

    @property
    def engine(self):
        return self.__engine

    @property
    def session(self):
        return self.__session

    @property
    def metadata(self):
        return self.__metadata

    @property
    def tables_in_db(self):
        self.__metadata.reflect()
        return self.__metadata.tables

    @allure.step("Describing table {1}")
    def describe_table(self, table_name: str):
        structure = Table(table_name, self.__metadata, with_autoload=True)
        allure.attach(str(tuple(structure.columns)), f"Structure of table '{table_name}'")

        return structure

    @allure.step("Creating table from the model")
    def create_table_model(self, dict_model: dict):
        tbl_model = Table(dict_model["name"], self.__metadata, *dict_model["columns"])
        allure.attach(str(tuple(tbl_model.columns)), f"Expected structure of table '{dict_model['name']}'")

        return tbl_model

    @allure.step("Inserting test data into table {1} from test file '{2}'")
    def insert_test_data(self, table_name: str, path_to_csv: str):
        with open(path_to_csv) as tmp:
            allure.attach(tmp, "Test data", allure.attachment_type.CSV)
        fn = tuple(
            dict(item[1]) for item in tuple(pandas.read_csv(path_to_csv, sep=';', header=0, index_col=None).iterrows()))
        tbl = Table(table_name, self.__metadata, with_autoload=True)
        self.__engine.execute(tbl.insert(), fn)
        return self

    @allure.step("Drop Database {1}")
    def drop_database(self, db_name):
        drop_database(self.__engine)
