import os
import subprocess

import allure


class CommandAdapter:
    def __init__(self, command: str, params: dict, host, port, user, password):
        self.host = host
        self.port = port
        self.user = user
        self.password = password
        self.command = command
        self.params = params

    def __call__(self):
        additional_params = " ".join(
            [f"--{k} {v}" for k, v in zip(self.params.keys(), self.params.values())]) if self.params else ""
        cmd = f'{self.command} {additional_params} --host {self.host} --port {self.port} --username {self.user} --password {self.password}'
        with allure.step(f"Executing command: {cmd}"):
            res = subprocess.run([cmd], shell=True)
            allure.attach(str(res.stdout), "Command Output", allure.attachment_type.TEXT)
            allure.attach(str(res.stderr), "Command errors", allure.attachment_type.TEXT)

            return res


class CreateDatabaseMetadata(CommandAdapter):
    def __init__(self, host, port, user, password, database):
        super().__init__(f"python {os.path.join('..', 'create_database_metadata.py')}", {"database": database}, host,
                         port, user, password)
