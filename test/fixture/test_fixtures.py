import pytest

from test.adapters.command_adapter import CreateDatabaseMetadata
from test.adapters.mysql_adapter import MySQLAdapter


@pytest.fixture(scope="session")
def mysql_adapter(request):
    host = request.config.getoption("--host")
    port = request.config.getoption("--port")
    user = request.config.getoption("--user")
    password = request.config.getoption("--password")
    database = request.config.getoption("--database")

    mysql_client = MySQLAdapter(host=host, port=port, user=user, password=password, database=database)

    yield mysql_client


    mysql_client.session.close()



@pytest.fixture(scope="session")
def create_db_command(request):
    host = request.config.getoption("--host")
    port = request.config.getoption("--port")
    user = request.config.getoption("--user")
    password = request.config.getoption("--password")
    database = request.config.getoption("--database")

    client = CreateDatabaseMetadata(host, port, user, password, database)

    return client()