from test.data_models.tables_models import table_models


def pytest_addoption(parser):
    parser.addoption("--host",
                     action='store',
                     help="Hostname of the MySQL server")
    parser.addoption("--port",
                     action='store',
                     help="Port of the MySQL server")
    parser.addoption("--user",
                     action='store',
                     help="Username of the MySQL server")
    parser.addoption("--password",
                     action='store',
                     help="Pasword of the MySQL server")
    parser.addoption("--database",
                     action='store',
                     help="Database to use")


def pytest_generate_tests(metafunc):
    if "expected_table_model" in metafunc.fixturenames:
        metafunc.parametrize("expected_table_model", table_models)
