from subprocess import CompletedProcess

import allure
import pytest

from test.adapters.mysql_adapter import MySQLAdapter


@allure.suite("Commands work test")
class TestCommands:
    create_db_command = 0

    @pytest.mark.create_db
    @allure.title("Verify that `create_database_metadata` works as expected")
    def test_create_db(self, mysql_adapter: MySQLAdapter, create_db_command: CompletedProcess,
                       expected_table_model: dict):
        if not self.__class__.create_db_command:
            assert not create_db_command.returncode, f"Command has finished with error code: {create_db_command.returncode}"
            self.__class__.create_db_command += 1
        expected_model = mysql_adapter.create_table_model(expected_table_model)
        actual_model = mysql_adapter.describe_table(expected_table_model["name"])

        assert expected_model == actual_model, f"Expected and actual sctructures of the table '{expected_table_model['name']}' are not equal"
