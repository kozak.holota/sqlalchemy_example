from sqlalchemy import Column, ForeignKey
from sqlalchemy.dialects.mysql import VARCHAR, FLOAT, INTEGER

item = {
    "name": "item",
    "columns": (
        Column('ID', INTEGER, primary_key=True, autoincrement=True),
        Column('ITEM_NAME', VARCHAR(150)),
        Column('PRICE', FLOAT)
    )
}

shop = {
    "name": "shop",
    "columns": (
        Column('ID', INTEGER, primary_key=True, autoincrement=True),
        Column('NAME', VARCHAR(150)),
        Column('ADDRESS', VARCHAR(250)),
    )
}

item_shop = {
    "name": "item_shop",
    "columns": (
        Column('ID', INTEGER, primary_key=True, autoincrement=True),
        Column('SHOP_ID', INTEGER, ForeignKey('shop.ID', ondelete='cascade')),
        Column('ITEM_ID', INTEGER, ForeignKey('item.ID', ondelete='cascade'))
    )
}


table_models = ((item), (shop), (item_shop))