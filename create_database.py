import click
from sqlalchemy import create_engine
from sqlalchemy_utils import database_exists, create_database
from data_models.item import Item
from data_models.item_shop import ItemShop
from data_models.shop import Shop


@click.command()
@click.option('--host', default="localhost", help='MySQL to connect to')
@click.option('--port', default=3306, help='MySQL to connect to')
@click.option('--username', help='Database username')
@click.option('--password', help='Database password')
@click.option('--database', help='Database to create')
def _create_database(host, port, username, password, database):
    # Create DB engine
    mysql_engine = create_engine(f'mysql+pymysql://{username}:{password}@{host}:{str(port)}/{database}')

    #Check if database already exists using database_exists() from sqlalchemy_utils package
    if not database_exists(mysql_engine.url):
        # If doesn't exist, just create it with create_database() function from sqlalchemy_utils package
        create_database(mysql_engine.url)

    '''
    Each data model class derived from the Declarative Base class contains `metadata` object of Metadata class.
    Calling `create_all()`, we can create the table based on the data model class
    '''
    Item.metadata.create_all(mysql_engine)
    Shop.metadata.create_all(mysql_engine)
    ItemShop.metadata.create_all(mysql_engine)


if __name__ == "__main__":
    _create_database()
