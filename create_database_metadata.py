import click
from sqlalchemy import create_engine
from sqlalchemy_utils import database_exists, create_database

from data_models.base import meta
from data_models.tables_metadata import item_metadata, shop_metadata, item_shop_metadata


@click.command()
@click.option('--host', default="localhost", help='MySQL to connect to')
@click.option('--port', default=3306, help='MySQL to connect to')
@click.option('--username', help='Database username')
@click.option('--password', help='Database password')
@click.option('--database', help='Database to create')
def _create_database(host, port, username, password, database):
    # Create DB engine
    mysql_engine = create_engine(f'mysql+pymysql://{username}:{password}@{host}:{str(port)}/{database}')

    # Check if database already exists using database_exists() from sqlalchemy_utils package
    if not database_exists(mysql_engine.url):
        # If doesn't exist, just create it with create_database() function from sqlalchemy_utils package
        create_database(mysql_engine.url)

    '''
    Creating tables via Metadata object through Table objects
    '''

    # Create some tables
    # meta.create_all(bind=mysql_engine, tables=[item_metadata, shop_metadata])

    # Create all tables from the metadata
    meta.create_all(bind=mysql_engine)


if __name__ == "__main__":
    _create_database()
