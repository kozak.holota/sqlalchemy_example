from sqlalchemy import Table, Column, Integer, String, Float, ForeignKey

from data_models.base import meta

item_metadata = Table('item', meta,
                      Column('ID', Integer, primary_key=True, autoincrement=True),
                      Column('ITEM_NAME', String(150)),
                      Column('PRICE', Float)
                      )

shop_metadata = Table('shop', meta,
                      Column('ID', Integer, primary_key=True, autoincrement=True),
                      Column('NAME', String(150)),
                      Column('ADDRESS', String(250))
                      )

item_shop_metadata = Table('item_shop', meta,
                           Column('ID', Integer, primary_key=True, autoincrement=True),
                           Column('SHOP_ID', Integer, ForeignKey('shop.ID', ondelete='cascade')),
                           Column('ITEM_ID', Integer, ForeignKey('item.ID', ondelete='cascade'))
                           )
