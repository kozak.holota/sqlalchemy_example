from sqlalchemy import Column, Integer, String

from data_models.base import Base


class Shop(Base):
    __tablename__ = 'shop'

    ID = Column(Integer, primary_key=True, autoincrement=True)
    NAME = Column(String(150))
    ADDRESS = Column(String(250))
