from sqlalchemy import Column, Integer, ForeignKey

from data_models.base import Base


class ItemShop(Base):
    __tablename__ = 'item_shop'

    ID = Column(Integer, primary_key=True, autoincrement=True)
    SHOP_ID = Column(Integer, ForeignKey('shop.ID', ondelete='cascade'))
    ITEM_ID = Column(Integer, ForeignKey('item.ID', ondelete='cascade'))
