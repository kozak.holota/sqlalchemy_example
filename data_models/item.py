from sqlalchemy import Column, Integer, String, Float

from data_models.base import Base


class Item(Base):
    __tablename__ = 'item'
    ID = Column(Integer, primary_key=True, autoincrement=True)
    ITEM_NAME = Column(String(150))
    PRICE = Column(Float)