import os

import click
from sqlalchemy import create_engine, select
from sqlalchemy import insert

from data_models.item import Item
from data_models.item_shop import ItemShop
from data_models.shop import Shop
from helper import read_csv


@click.command()
@click.option('--host', default="localhost", help='MySQL to connect to')
@click.option('--port', default=3306, help='MySQL to connect to')
@click.option('--username', help='Database username')
@click.option('--password', help='Database password')
@click.option('--database', help='Database to create')
def insert_data(host, port, username, password, database):
    # Create DB engine
    mysql_engine = create_engine(f'mysql+pymysql://{username}:{password}@{host}:{str(port)}/{database}')

    # Create connection derived from engine
    conn = mysql_engine.connect()

    # Read CSV from file into dictionary
    items = read_csv(os.path.join("test_data", "items.csv"))

    for item in items:
        # Insert each data from the data file into the table 'item'
        conn.execute(insert(Item).values(ITEM_NAME = item["ITEM_NAME"], PRICE = item["PRICE"]))

    #The same as above for the table 'shop'
    shops = read_csv(os.path.join("test_data", "shops.csv"))
    for shop in shops:
        conn.execute(insert(Shop).values(NAME=shop["NAME"], ADDRESS=shop["ADDRESS"]))

    '''
    Get items and shops from 'items_shops.csv'. There are 2 columns: 'ITEM' and 'SHOP'
    'ITEM' represents index of the item in the 'items.csv'
    'SHOP' represents index of the shop in the 'shops.csv'
    '''
    items_shops = read_csv(os.path.join("test_data", "items_shops.csv"))

    for i_s in items_shops:
        # Get data about the item by index
        item = items[i_s["ITEM"]]
        # Get data about the shop by index
        shop = shops[i_s["SHOP"]]

        #Get item ID from DB by ITEM_NAME
        item_id = conn.execute(select(Item.ID).where(Item.ITEM_NAME == item["ITEM_NAME"])).scalar()

        #Get shop ID from DB by NAME and ADDRESS
        shop_id = conn.execute(select(Shop.ID).where(Shop.NAME == shop["NAME"] and Shop.ADDRESS == shop["ADDRESS"])).scalar()

        conn.execute(insert(ItemShop).values(SHOP_ID=shop_id, ITEM_ID=item_id))

if __name__ == "__main__":
    insert_data()