import sys

import click
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from data_models.item import Item


@click.command()
@click.option('--host', default="localhost", help='MySQL to connect to')
@click.option('--port', default=3306, help='MySQL to connect to')
@click.option('--username', help='Database username')
@click.option('--password', help='Database password')
@click.option('--database', help='Database to create')
@click.option('--id', help='ID of the Item')
@click.option('--field_name', help='Field to change')
@click.option('--value', help='Field value')
def update_item(host, port, username, password, database, id, field_name, value):
    # Create DB engine
    mysql_engine = create_engine(f'mysql+pymysql://{username}:{password}@{host}:{str(port)}/{database}')

    # Create a session for several SQL transactions
    Session = sessionmaker(bind=mysql_engine)
    db_session = Session()

    item = db_session.query(Item).get(int(id))

    if not item:
        print(f"No item found with ID {id}")
        sys.exit(1)

    fields = ["ITEM_NAME", "PRICE"]

    if field_name not in fields:
        print(f"Field name '{field_name}' is invalid. Possible fields: {', '.join(fields)}")
        sys.exit(1)

    val = float(value) if field_name == "PRICE" else value

    #To update something in some table, just set the proper attribute in the data model object to another value
    #At the end of the day it will look like: item.PRICE = 477
    setattr(item, field_name, val)

    db_session.commit()


if __name__ == "__main__":
    update_item()
