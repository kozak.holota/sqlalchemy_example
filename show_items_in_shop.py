import click
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from data_models.item import Item
from data_models.item_shop import ItemShop
from data_models.shop import Shop


@click.command()
@click.option('--host', default="localhost", help='MySQL to connect to')
@click.option('--port', default=3306, help='MySQL to connect to')
@click.option('--username', help='Database username')
@click.option('--password', help='Database password')
@click.option('--database', help='Database to create')
@click.option('--shop_name', help='Set shop name to search assortiment in')
@click.option('--shop_address', help='Shop address')
def show_items_in_shop(host, port, username, password, database, shop_name, shop_address):
    # Create DB engine
    mysql_engine = create_engine(f'mysql+pymysql://{username}:{password}@{host}:{str(port)}/{database}')

    # Create a session for several SQL transactions
    Session = sessionmaker(bind=mysql_engine)
    db_session = Session()

    items_query = db_session.query(Item).join(ItemShop, Item.ID == ItemShop.ITEM_ID, isouter=True).join(Shop,
                                                                                                   Shop.ID == ItemShop.SHOP_ID, isouter=True)\
        .filter(Shop.NAME == shop_name).filter(Shop.ADDRESS == shop_address)

    print(items_query.statement.compile(mysql_engine, compile_kwargs={"literal_binds": True}))

    result = items_query.all()

    if not result:
        print("No shop is found or no items in it")
    else:
        for it in result:
            print(f"Item name: {it.ITEM_NAME}. Price: {str(it.PRICE)}")


if __name__ == "__main__":
    show_items_in_shop()
