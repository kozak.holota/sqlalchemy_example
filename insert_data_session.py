import os

import click
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from data_models.item import Item
from data_models.item_shop import ItemShop
from data_models.shop import Shop
from helper import read_csv


@click.command()
@click.option('--host', default="localhost", help='MySQL to connect to')
@click.option('--port', default=3306, help='MySQL to connect to')
@click.option('--username', help='Database username')
@click.option('--password', help='Database password')
@click.option('--database', help='Database to create')
def insert_data(host, port, username, password, database):
    # Create DB engine
    mysql_engine = create_engine(f'mysql+pymysql://{username}:{password}@{host}:{str(port)}/{database}')

    # Create a session for several SQL transactions
    Session = sessionmaker(bind=mysql_engine)
    db_session = Session()

    # items = [Item(ITEM_NAME=item["ITEM_NAME"], PRICE=item["PRICE"]) for item in
    #          read_csv(os.path.join("test_data", "items.csv"))]
    items = [Item(**item) for item in read_csv(os.path.join("test_data", "items.csv"))]

    # shops = [Shop(NAME=shop["NAME"], ADDRESS=shop["ADDRESS"]) for shop in
    #          read_csv(os.path.join("test_data", "shops.csv"))]
    shops = [Shop(**shop) for shop in read_csv(os.path.join("test_data", "shops.csv"))]


    db_session.add_all(items + shops)

    items_shops = read_csv(os.path.join("test_data", "items_shops.csv"))

    for i_s in items_shops:
        # Get data about the item by index
        item = items[i_s["ITEM"]]
        # Get data about the shop by index
        shop = shops[i_s["SHOP"]]

        # Get item ID from DB by ITEM_NAME
        item_id = db_session.query(Item.ID).filter(Item.ITEM_NAME == item.ITEM_NAME).scalar()

        # Get shop ID from DB by NAME and ADDRESS
        shop_id = db_session.query(Shop.ID).filter(Shop.NAME == shop.NAME).filter(Shop.ADDRESS == shop.ADDRESS).scalar()

        db_session.add(ItemShop(SHOP_ID=shop_id, ITEM_ID=item_id))

    db_session.commit()


if __name__ == "__main__":
    insert_data()
